<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <title>Chat</title>
</head>
<body>
    <div class="container" >

        <div class="row" id="chat_holder">
            <div class="chat-inner-box col-4 offset-4">
                <li class="list-group-item active">Chat room</li>
                <ul class="list-group chat-message-group" v-chat-scroll>

                    <message
                            v-for="value in chat.messages"
                            :key="value.index"
                            color="success"
                    >
                        @{{ value }}
                    </message>

                </ul>
                <input type="text" class="form-control" placeholder="your message"
                       v-model='message' @keyup.enter='send'
                >
            </div>
        </div>
    </div>
    <script src="{{asset('js/app.js')}}"></script>
</body>
</html>